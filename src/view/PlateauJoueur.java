package view;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.*;

import controller.PlateauJoueurController;
import model.Bateau;
import model.Jeu;
import model.Plateau;
import start.Lanceur;
import view.Menu;

public class PlateauJoueur extends JPanel implements Observer {

	JButton plateau[][];
	PlateauJoueurController pjc;
	Jeu jeu;
	JLabel console;
	JPanel grille;

	public static BufferedImage GameOver;
	public static BufferedImage GameWin;

	public PlateauJoueur(Jeu j2) {
		try {
			GameOver = ImageIO.read(Lanceur.class.getResource("/Img/Gameover.jpg"));
			GameWin = ImageIO.read(Lanceur.class.getResource("/Img/Win.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		jeu = j2;
		pjc = new PlateauJoueurController(j2);
		this.setLayout(new BorderLayout());
		this.add(new JLabel("Votre plateau"), BorderLayout.NORTH);
		grille = new JPanel();
		plateau = new JButton[11][11];
		// Le premier bouton est vide
		JButton aAjouter = new JButton("");
		aAjouter.setEnabled(false);
		plateau[0][0] = aAjouter;

		// On creer la première ligne du plateau
		for (int i = 1; i < 11; i++) {
			aAjouter = new JButton("" + i);
			aAjouter.setEnabled(false);
			plateau[0][i] = aAjouter;
		}

		// On creer la première colonne du plateau
		for (int i = 1; i < 11; i++) {
			aAjouter = new JButton("" + i);
			aAjouter.setEnabled(false);
			plateau[i][0] = aAjouter;
		}

		// on ajoute les autres cases
		for (int i = 1; i < 11; i++)
			for (int j = 1; j < 11; j++) {
				aAjouter = new JButton("");
				aAjouter.setEnabled(true);
				aAjouter.setActionCommand(i + ":" + j);
				aAjouter.addActionListener(pjc);

				plateau[i][j] = aAjouter;
			}

		for (JButton[] b1 : plateau)
			for (JButton b2 : b1){
				grille.add(b2);
			}

		//On creer une console pour afficher quelques informations
		console = new JLabel();
		GridLayout experimentLayout = new GridLayout(11,11);
		grille.setLayout(experimentLayout);
		this.add(grille, BorderLayout.CENTER);
		this.add(console, BorderLayout.SOUTH);
		j2.addObserver(this);
	}

	@Override
	public void update(Observable arg0, Object arg1) {

		if (jeu.getEtatPartie().equals(Jeu.ETAT_PARTIE_FINI)){
			this.afficherFin();
		} else {
			switch (jeu.getEtatPartie()) {
			case Jeu.ETAT_JOUEUR_PLACER_BATEAU:
				//On récupère le bateau a placer
				Bateau b = jeu.recupererBateauAPlacer();
				console.setText("Veuillez selectionner deux cases a "+b.getTaille()+" cases pour placer le "+b);
				break;
			case Jeu.ETAT_JOUEUR_SELECTIONNER_BATEAU:
				//On demande de selectionner un bateau
				console.setText("Veuillez selectionner le bateau tireur");
				break;
			case Jeu.ETAT_JOUEUR_TIRER_BATEAU:
				//On demande demande au bateau de tirer
				b = jeu.getBateauSelected();
				console.setText("Votre "+b+" a encore "+b.getNbProjectiles()+" projectiles. Ou souhaitez vous tirer ?");
				break;
			default:
			
				break;
			}

			affichageBateau();
			affichageCoupRecu();

			grille.removeAll();
			// Redraw
			for (JButton[] b1 : plateau)
				for (JButton b2 : b1)
					grille.add(b2);

			this.repaint();
		}

	}

	private void afficherFin() {
		if (jeu.getPerdant().equals(Jeu.ETAT_PERDU_JOUEUR))
			this.getGraphics().drawImage(GameOver, 0, 0, getWidth(), getHeight(), null);
		else
			this.getGraphics().drawImage(GameWin, 0, 0, getWidth(), getHeight(), null);

	}

	public void affichageBateau() {
		// On récupère les bateaux et on les affiches si on est dans la phase de
		// placement de bateaux
		ArrayList<Bateau> bateaux = jeu.getBateauxJoueur();
		boolean bateauSelect = false;
		for (Bateau b : bateaux) {
			if (b == jeu.getBateauSelected())
				bateauSelect = true;
			if (b.getPos()[0] != null) {
				Point[] avecUnBateau = b.getPos();
				for (Point p : avecUnBateau) {
					JButton butt = plateau[p.x + 1][p.y + 1];
					if (bateauSelect)
						butt.setForeground(Color.RED);
					else
						butt.setForeground(Color.BLACK);
					butt.setText("B");
					plateau[p.x + 1][p.y + 1] = butt;
				}
			}
			bateauSelect = false;
		}
	}

	public void affichageCoupRecu() {
		Plateau pj = jeu.getPlateau();
		HashMap<Point, String> hm = pj.getListCoupTire();
		Set cles = hm.keySet();
		Iterator it = cles.iterator();
		while (it.hasNext()) {
			Point p = (Point) it.next();
			String valeur = hm.get(p);
			JButton butt = plateau[p.x + 1][p.y + 1];
			switch (valeur) {
			case Plateau.PLOUF:
				butt.setForeground(Color.BLUE);
				butt.setText("O");
				break;
			case Plateau.TOUCHE:
				butt.setForeground(Color.RED);
				butt.setText("T");
				break;
			case Plateau.COULE:
				butt.setForeground(Color.BLACK);
				butt.setText("X");
				break;
			}

			plateau[p.x + 1][p.y + 1] = butt;
		}
		
	}

}
