package view;

import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import javax.swing.*;

import controller.PlateauAdversaireController;
import model.Jeu;
import model.Plateau;
import view.Menu;

public class PlateauAdversaire extends JPanel implements  Observer{

	JButton plateau[][];
	Jeu jeu;
	PlateauAdversaireController pac;

	public PlateauAdversaire(Jeu j2) {
		jeu = j2;
		pac = new PlateauAdversaireController(j2);
		this.setLayout(new BorderLayout());
		this.add(new JLabel("Le plateau de votre adversaire"), BorderLayout.NORTH);
		JPanel grille = new JPanel();
		plateau = new JButton[11][11];
		//Le premier bouton est vide
		JButton aAjouter = new JButton("");
		aAjouter.setEnabled(false);
		plateau[0][0] = aAjouter;

		//On creer la première ligne du plateau
		for(int i = 1; i < 11; i++) {
			aAjouter = new JButton(""+i);
			aAjouter.setEnabled(false);
			plateau[0][i] = aAjouter;
		}


		//On creer la première colonne du plateau
		for(int i = 1; i < 11; i++) {
			aAjouter = new JButton(""+i);
			aAjouter.setEnabled(false);
			plateau[i][0] = aAjouter;
		}


		//on ajoute les autres cases
		for(int i = 1; i < 11; i++)
			for(int j = 1; j < 11; j++) {
				aAjouter = new JButton("");
				aAjouter.addActionListener(pac);
				aAjouter.setActionCommand(i+","+j);
				plateau[i][j] = aAjouter;
			}



		for(JButton[] b1 : plateau)
			for(JButton b2 : b1)
				grille.add(b2);

		GridLayout experimentLayout = new GridLayout(11,11);
		grille.setLayout(experimentLayout);
		this.add(grille, BorderLayout.CENTER);
		j2.addObserver(this);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		switch(jeu.getEtatPartie()) {
		case Jeu.ETAT_JOUEUR_TIRER_BATEAU:
			this.setEnabled(true);
			break;
		default:
			this.setEnabled(false);
			break;
		}

		affichageCoupRecu();

		this.repaint();
	}

	public void affichageCoupRecu() {
		Plateau pj = jeu.getPlateauMachine();
		HashMap<Point, String> hm = pj.getListCoupTire();
		Set cles = hm.keySet();
		Iterator it = cles.iterator();
		while (it.hasNext()){
			Point p = (Point) it.next(); 
			String valeur = hm.get(p);
			JButton butt = plateau[p.x+1][p.y+1];
			switch(valeur) {
			case Plateau.PLOUF:
				butt.setForeground(Color.BLUE);
				butt.setText("O");
				butt.removeActionListener(pac);
				break;
			case Plateau.TOUCHE:
				butt.setForeground(Color.RED);
				butt.setText("T");
				butt.removeActionListener(pac);
				break;
			case Plateau.COULE:
				butt.setForeground(Color.BLACK);
				butt.setText("X");
				butt.removeActionListener(pac);
				break;
			}
			
			
			plateau[p.x+1][p.y+1] = butt;
		}
	}

}
