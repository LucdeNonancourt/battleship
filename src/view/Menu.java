package view;

import javax.swing.*;

import model.FlotteFactory;
import model.Jeu;

import java.awt.*;
import java.awt.event.*;

public class Menu extends JDialog {

	JTextArea selection;
	JButton bouton;
	JButton charger;
	JComboBox epoque;
	JFrame jeu;
	Jeu j;

	public Menu(JFrame fenetreJoueur, Jeu j) {

		this.setLayout(new BorderLayout());
		selection = new JTextArea("Veuillez selectionner une époque");
		bouton = new JButton("Lancer le jeu !");
		charger = new JButton("Charger une partie sauvegardée");
		String[] items = { FlotteFactory.FLOTTE_XX, FlotteFactory.FLOTTE_XV };
		epoque = new JComboBox(items);
		bouton.addActionListener(new EpoqueSelectionne());
		charger.addActionListener(new ChargerPartie());
		this.add(selection, BorderLayout.NORTH);
		this.add(epoque, BorderLayout.CENTER);
		this.add(bouton, BorderLayout.SOUTH);

		jeu = fenetreJoueur;
		this.j = j;
	}

	public class EpoqueSelectionne implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String epoquestr = (String) epoque.getItemAt(epoque.getSelectedIndex());
			jeu.setVisible(true);
			j.initialise(epoquestr);
			dispose();
		}
	}

	public class ChargerPartie implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			jeu.setVisible(true);
			j.chargerPartie();
			dispose();
		}
	}

}