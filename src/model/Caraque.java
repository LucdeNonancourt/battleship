package model;

import java.awt.Point;

public class Caraque extends Bateau {

	/**
	 * Construit caraque aux positions donn� en parametres
	 */
	public Caraque() {
		this.libelle = "Caraque";
		this.vie = 2;
		this.puissance = 2;
		this.nbProjectile = 20;
		this.taille = 2;
		this.positions = new Point[taille];
	}
}
