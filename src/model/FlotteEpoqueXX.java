package model;

import java.util.ArrayList;

import dao.JeuDAO;
import dao.JeuTxtDAO;
import dao.TxtFactory;

public class FlotteEpoqueXX extends FlotteFactory {

	private static FlotteEpoqueXX instance = null;

	private FlotteEpoqueXX() {

	}

	public static FlotteEpoqueXX getInstance() {
		if (instance == null)
			instance = new FlotteEpoqueXX();
		return instance;
	}
	
	@Override
	protected ArrayList<Bateau> creerFlotte() {
		ArrayList<Bateau> flotte = new ArrayList<Bateau>();
		flotte.add(new Croisseur());
		flotte.add(new Croisseur());
		flotte.add(new Cuirasse());
		return flotte;
	}

}
