package model;

import java.util.ArrayList;

public class FlotteEpoqueXV extends FlotteFactory {
	
	private static FlotteEpoqueXV instance = null;

	private FlotteEpoqueXV() {

	}

	public static FlotteEpoqueXV getInstance() {
		if (instance == null)
			instance = new FlotteEpoqueXV();
		return instance;
	}

	@Override
	protected ArrayList<Bateau> creerFlotte() {
		ArrayList<Bateau> flotte = new ArrayList<Bateau>();
		flotte.add(new Galion());
		flotte.add(new Caraque());
		flotte.add(new Caraque());
		return flotte;
	}
	
}
