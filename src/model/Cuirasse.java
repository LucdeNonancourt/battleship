package model;

import java.awt.Point;

public class Cuirasse extends Bateau {

	/**
	 * Construit caraque aux positions donn� en parametres
	 */
	public Cuirasse() {
		this.libelle = "Cuirasse";
		this.vie = 3;
		this.puissance = 1;
		this.nbProjectile = 20;
		this.taille = 3;
		this.positions = new Point[taille];
	}
}
