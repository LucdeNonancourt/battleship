package model;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class Ordinateur {
	private int tailleGrille;
	private Point attaque;
	private Plateau plateau;
	private ArrayList<Point> coupsPossibleCroix;
	private ArrayList<Point> coupsPossibleAlea;
	private ArrayList<Point> placementPossible;

	private HashMap<Point, String> lisCoupTire;
	private ArrayList<Point> voisinP;

	public Ordinateur(Plateau plateau) {
		this.plateau = plateau;
		tailleGrille = Plateau.TAILLE_GRILLE;
		attaque = new Point();
		
		coupsPossibleCroix = new ArrayList<Point>();
		coupsPossibleAlea = new ArrayList<Point>();
		placementPossible = new ArrayList<Point>();
		voisinP = new ArrayList<Point>();
		lisCoupTire = new HashMap<Point, String>();
		int k = 0;
		for (int i = 0; i < tailleGrille; i++) {
			for (int j = 0; j < tailleGrille; j++) {
				coupsPossibleAlea.add(new Point(i, j));
				placementPossible.add(new Point(i, j));
				if (j % 2 == k)
					coupsPossibleCroix.add(new Point(i, j));
			}
			if (k == 0)
				k = 1;
			else
				k = 0;
		}
		
		attaque = null;
	}

	public Point attaqueEnCroix() {
        if (attaque != null) {
            if (lisCoupTire.get(attaque).equals(Plateau.TOUCHE)) {
                voisinP.clear();
                chasserBateau();
            } else chercherBateauEnCroix();
        }
        if (voisinP.size() != 0) attaque = voisinP.remove(0);
        else chercherBateauEnCroix();
        return attaque;
	}

	private void chercherBateauEnCroix() {
		int randomIndice = (int) (Math.random() * coupsPossibleCroix.size());
		attaque = coupsPossibleCroix.remove(randomIndice);
	}

	public Point attaqueAleatoire() {
        if (attaque != null) {
            if (lisCoupTire.get(attaque).equals(Plateau.TOUCHE)) {
                voisinP.clear();
                chasserBateau();
            } else chercherBateauAleatoire();
        }
        if (voisinP.size() != 0) attaque = voisinP.remove(0);
        else chercherBateauAleatoire();
        return attaque;
	}

	private void chercherBateauAleatoire() {
		int randomIndice = (int) (Math.random() * (coupsPossibleAlea.size()));
		attaque = coupsPossibleAlea.remove(randomIndice);
	}

	private void chasserBateau() {
		//creation des points voisins du tir 
		Point voisin0 = new Point(attaque.x, attaque.y + 1);
		Point voisin1 = new Point(attaque.x, attaque.y - 1);
		Point voisin2 = new Point(attaque.x - 1, attaque.y);
		Point voisin3 = new Point(attaque.x + 1, attaque.y);
		
		ArrayList<Point> voisin = new ArrayList<Point>();
		voisin.add(voisin0);
		voisin.add(voisin1);
		voisin.add(voisin2);
		voisin.add(voisin3);

		int randomIndice;
		Point p;
		boolean trouve = false;
		while (!trouve) {
			randomIndice = (int) (Math.random() * voisin.size());
			p = voisin.get(randomIndice);
			// verif point pas hors limite
			if (p.getX() >= 0 && p.getX() < tailleGrille && p.getY() >= 0 && p.getY() < tailleGrille) {
				trouve = true;
				attaque = p;
			}
		}
	}

	public Point[] placerBateau(Bateau bateau) {
		Point[] res = new Point[2];
		Point p = null;
		do {
			do {
				int randomX = (int) (Math.random() * (tailleGrille));
				int randomY = (int) (Math.random() * (tailleGrille));
				res[0] = new Point(randomX, randomY);
				for (Bateau b : plateau.getListBateau()) {
					if (b.getPos()[0] != null) {
						for (int i = 0; i < b.getPos().length; i++) {
							if (b.getPos()[i].equals(res[0]))
								res[0] = null;
						}
					}
				}
			} while (res[0] == null);
			
            int alea = (int) (Math.random() * 4);
            if (res[0].x + bateau.taille - 1 < tailleGrille && alea == 0) {
                p = new Point(res[0].x + bateau.taille - 1, res[0].y);
            } else if (res[0].x - bateau.taille - 1 >= 0 && alea == 1) {
                p = new Point(res[0].x - bateau.taille - 1, res[0].y);
            } else if (res[0].y + bateau.taille - 1 < tailleGrille && alea == 2) {
                p = new Point(res[0].x, res[0].y + bateau.taille - 1);
            } else if (res[0].y - bateau.taille - 1 >= 0 && alea == 3) {
                p = new Point(res[0].x, res[0].y - bateau.taille - 1);
            }
            if (p != null && !plateau.verifierPasDejaBateau(bateau, res[0], p)) p = null;
		} while (p == null);

		res[1] = p;
		return res;
	}

	public Bateau choisirBateau() {
		ArrayList<Bateau> listeBateau = plateau.getListBateau();
		int randomIndice;
		do {
			randomIndice = (int) (Math.random() * listeBateau.size());
		} while (listeBateau.get(randomIndice).nbProjectile == 0);
		return listeBateau.get(randomIndice);
	}

	/**
	 * permet de rajouter l'etat du dernier tir qu'on a effectu� dans la
	 * hashmap @lisCoupTire
	 * 
	 * @param etatTir, touche, coule, plouf
	 */
	public void notifCoup(String etatTir) {
		lisCoupTire.put(attaque, etatTir);
	}
}
