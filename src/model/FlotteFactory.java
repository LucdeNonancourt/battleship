package model;

import java.util.ArrayList;

public abstract class FlotteFactory {
	
	public static final String FLOTTE_XX = "epoqueXX";
	public static final String FLOTTE_XV = "epoqueXV";


	public ArrayList<Bateau> formerFlotte(){
		ArrayList<Bateau> flotte = this.creerFlotte();
		return flotte;
	}

	protected abstract ArrayList<Bateau> creerFlotte();
}
