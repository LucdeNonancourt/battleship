package model;

import java.awt.Point;

public abstract class Bateau {

	/**
	 * Nom du bateau
	 */
	protected String libelle;

	/**
	 * Taille en nombre de case du bateau
	 */
	protected int taille;

	/**
	 * Point de vie du bateau
	 */
	protected int vie;

	/**
	 * Puissance de feu du bateau
	 */
	protected int puissance;

	/**
	 * Nombre de projectile qui reste au bateau
	 */
	protected int nbProjectile;

	/**
	 * Position point par point du bateau
	 */
	protected Point[] positions;

	/**
	 * Methode qui fait perdre des points de vie au bateau et fait sombrer le bateau
	 * quand les points de vie sont nulles
	 * 
	 * @param degat, nombre de point de vie a retirer au bateau
	 */
	public void perdreVie(int degat) {
		if (degat > 0) {
			if (this.vie - degat > 0) {
				this.vie -= degat;
			} else {
				this.vie = 0;
			}
		}
	}

	/**
	 * Permet de savoir si le bateau est coule ou pas @return, vrai si les points de
	 * vie sont a 0, faux sinon
	 */
	public boolean etreCoule() {
		return (this.vie == 0);
	}

	/**
	 * Methode qui retire 1 projectile de notre reserve et renvoie les degats du tir
	 * 
	 * @return degat caus� par le tir du bateau
	 */
	public int tirerBoulet() {
		if (this.nbProjectile > 0) {
			this.nbProjectile--;
			return this.puissance;
		}
		return 0;
	}

	/**
	 * Methode qui permet de positionner le bateau donn� a partir de 2
	 * points aux extremites
	 * @param pos, tableau de point ou se trouve le bateau
	 * @return true si on a pu placer le bateau, false sinon
	 */
	public boolean positionnerBateau(Point[] pos) {
		boolean placer = false;
		Point a = pos[0];
		Point b = pos[1];
		Point[] tabP = trouverPointFrom2Point(a, b);
		if (tabP != null) {
			placer = true;
			for (int i = 0; i < positions.length; i++) {
				positions[i] = tabP[i];
			}
		}
		return placer;
	}

	/**
	 * Permet de creer un tableau a de point pour les positions du bateau
	 * depuis 2 points (Uniquement un croix, ne comprend pas les diagonales)
	 * @param a, premier point
	 * @param b, deuxieme point
	 * @return le tableau de point correspondant a la position du bateau, 
	 * un tableau null si on ne peut pas placer les points
	 */
	public Point[] trouverPointFrom2Point(Point a, Point b) {
		Point[] tabP = null;
		// cas x entre 2 point ==
		if (a.getX() == b.getX()) {
			if (a.getY() < b.getY()) {
				tabP = trouverPointY(a, b);
			} else {
				tabP = trouverPointY(b, a);
			}
			// cas y entre 2 point ==
		} else if (a.getY() == b.getY()) {
			if (a.getX() < b.getX()) {
				tabP = trouverPointX(a, b);
			} else {
				tabP = trouverPointX(b, a);
			}
		}
		return tabP;
	}
	
	

	/**
	 * trouve les points entre 2 points sur l'axe des x
	 * 
	 * @param x1, premier point (min)
	 * @param x2, deuxieme point (max)
	 * @return tout les points
	 */
	private Point[] trouverPointX(Point x1, Point x2) {
		Point[] tabP = null;
		// verif placement autorise
		if (x2.getX() - x1.getX() < this.taille) {
			tabP = new Point[taille];
			int j = 0;
			for (int i = (int) x1.getX(); i <= x2.getX(); i++) {
				tabP[j] = new Point(i, (int) x1.getY());
				j++;
			}
		}
		return tabP;
	}

	/**
	 * trouve les points entre 2 points sur l'axe des y
	 * 
	 * @param x1,premier point (min)
	 * @param x2, deuxieme point (max)
	 * @return tout les points
	 */
	private Point[] trouverPointY(Point x1, Point x2) {
		Point[] tabP = null;
		// verfi placement autorise
		if (x2.getY() - x1.getY() < this.taille) {
			tabP = new Point[taille];
			int j = 0;
			for (int i = (int) x1.getY(); i <= x2.getY(); i++) {
				tabP[j] = new Point((int) x1.getX(), i);
				j++;
			}
		}
		return tabP;
	}

	public String toString() {
		return this.libelle;
	}

	public int getVie() {
		return this.vie;
	}

	public int getNbProjectiles() {
		return this.nbProjectile;
	}

	public Point[] getPos() {
		return this.positions;
	}

	public int getTaille() {
		return this.taille;
	}

}
