package model;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

public class Jeu extends Observable {

	public static final String ETAT_JOUEUR_PLACER_BATEAU = "placer_bateau";
	public static final String ETAT_JOUEUR_SELECTIONNER_BATEAU = "recup_bateau";
	public static final String ETAT_JOUEUR_TIRER_BATEAU = "tirer_bateau";
	public static final String ETAT_PARTIE_FINI = "fini";
	public static final String ETAT_PERDU_JOUEUR = "perdu_joueur";

	/**
	 * Plateau de l'IA
	 */
	private Plateau plateauMachine;

	/**
	 * Plateau du joueur
	 */
	private Plateau plateauJoueur;

	/**
	 * Bateau qui va effectuer le tir
	 */
	private Bateau bateauSelected;

	/**
	 * Etat de la partie (on attente de tir joueur,..., fini)
	 */
	private String etatPartie;

	/**
	 * Intelligence artificielle qui va jouer
	 */
	private Ordinateur iaJeu;

	/**
	 * Donne le perdant
	 */
	private String perdant;

	/**
	 * Methode qui permet de placer les bateaux sur un plateau une fois le placement
	 * fini on change @etatPartie
	 * 
	 * @param listBatoPosition, liste de couple bateau et tableau de point de la
	 *        postion du bateau
	 */
	public void placerBateauxJoueur(Bateau b, Point[] tabP) {
		if (etatPartie.equals(ETAT_JOUEUR_PLACER_BATEAU)) {
			this.plateauJoueur.placerBateau(b, tabP);
			if (plateauJoueur.estFiniPlacerbateau()) {
				this.etatPartie = ETAT_JOUEUR_SELECTIONNER_BATEAU;
			}
			this.setChanged();
			this.notifyObservers();
		}
	}

	/**
	 * Permet de selectionner un bateau du joueur passe le bateau a
	 * l'attribut @bateauSelected et change @etatPartie
	 * 
	 * @param pos, position du clic
	 * @return vrai si il y a un bateau a cette position, faux sinon
	 */
	public boolean recupererBateau(Point pos) {
		boolean trouve = false;
		Bateau b = plateauJoueur.chercherBateau(pos);
		if (b != null) {
			if(b.getNbProjectiles() > 0) {
				bateauSelected = b;
				trouve = true;
				this.etatPartie = ETAT_JOUEUR_TIRER_BATEAU;
				this.setChanged();
				this.notifyObservers();
			}
		}
		return trouve;
	}

	public void jouerCoup(Point caseCible) {
		if (caseCible != null && !etatPartie.equals(ETAT_PARTIE_FINI)) {
			// Joueur physique joue
			plateauMachine.jouer(caseCible, bateauSelected);
			this.etatPartie = ETAT_JOUEUR_SELECTIONNER_BATEAU;
			bateauSelected = null;

			// verif partie pas finie
			this.verifPartieFini();

			this.setChanged();
			this.notifyObservers();
		}
		if (!etatPartie.equals(ETAT_PARTIE_FINI)) {
			// Machine joue
			// recup point et bateau
			Point p = iaJeu.attaqueAleatoire();
			Bateau b = iaJeu.choisirBateau();
			String etatTir = plateauJoueur.jouer(p, b);
			iaJeu.notifCoup(etatTir);

			// verif partie pas finie
			this.verifPartieFini();

			this.setChanged();
			this.notifyObservers();
		}
	}

	/**
	 * Permet de savoir si une partie est finie ou non finie si un des plateaux n'a
	 * plus de bateau(tous coul�) si oui lance la fini
	 * 
	 * @return
	 */
	public void verifPartieFini() {
		if (plateauJoueur.aPerdu()) {
			etatPartie = ETAT_PARTIE_FINI;
			perdant = ETAT_PERDU_JOUEUR;
		} else if (plateauMachine.aPerdu()) {
			etatPartie = ETAT_PARTIE_FINI;
		}
	}

	/**
	 * Methode d'initialisation du jeu, aprés avoir sélectionné l'époque dans le
	 * menu
	 * 
	 * @param epoque
	 */
	public void initialise(String epoque) {
		if (epoque.equals(FlotteFactory.FLOTTE_XV) || epoque.equals(FlotteFactory.FLOTTE_XX)) {
			etatPartie = ETAT_JOUEUR_PLACER_BATEAU;
			plateauMachine = new Plateau(epoque);
			plateauJoueur = new Plateau(epoque);
			iaJeu = new Ordinateur(plateauMachine);
			bateauSelected = null;
			perdant = new String();

			// Placer les bateaux de l'IA
			this.placerBateauxMachine();
			etatPartie = ETAT_JOUEUR_PLACER_BATEAU;
			this.setChanged();
			this.notifyObservers();
		}
	}

	/**
	 * Permet de placer les bateaux sur le plateau de la machine
	 */
	private void placerBateauxMachine() {
		for (Bateau b : plateauMachine.getListBateau()) {
			plateauMachine.placerBateau(b, iaJeu.placerBateau(b));
		}
	}

	/**
	 * Methode qui retourne la liste des bateaux du joueur (physique)
	 * 
	 * @return liste de Bateau
	 */
	public ArrayList<Bateau> getBateauxJoueur() {
		return this.plateauJoueur.getListBateau();
	}

	public Plateau getPlateauMachine() {
		return plateauMachine;
	}

	public void setPlateauMachine(Plateau plateauMachine) {
		this.plateauMachine = plateauMachine;
	}

	public Plateau getPlateau() {
		return plateauJoueur;
	}

	public void setPlateauJoueur(Plateau plateauJoueur) {
		this.plateauJoueur = plateauJoueur;
	}

	public Bateau getBateauSelected() {
		return bateauSelected;
	}

	public void setBateauSelected(Bateau bateauSelected) {
		this.bateauSelected = bateauSelected;
	}

	public String getEtatPartie() {
		return etatPartie;
	}

	public void setEtatPartie(String etatPartie) {
		this.etatPartie = etatPartie;
	}

	public Ordinateur getIaJeu() {
		return iaJeu;
	}

	public void setIaJeu(Ordinateur iaJeu) {
		this.iaJeu = iaJeu;
	}
	
	public String getPerdant() {
		return this.perdant;
	}
	
	public Bateau recupererBateauAPlacer() {
		ArrayList<Bateau> bateaux = getBateauxJoueur();
		for(Bateau b : bateaux) {
			if(b.getPos()[0] == null) {
				return b;
			}
		}
		return null;
	}

	public void chargerPartie() {
		
	}
	
	public void sauvegarderPartie() {
		// TODO Auto-generated method stub
		
	}
}
