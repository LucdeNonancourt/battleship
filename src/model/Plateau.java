package model;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Plateau {

	public static final int TAILLE_GRILLE = 10;
	public static final String TOUCHE = "touche";
	public static final String PLOUF = "plouf";
	public static final String COULE = "coule";

	/**
	 * Liste des bateaux plac�s sur la grille
	 */
	private ArrayList<Bateau> listBateau;

	/**
	 * Grille du jeu
	 */
	private Object[][] grille;

	/**
	 * Liste avec la position des coups tir� et l'information si c'est touch� ou
	 * plouf
	 */
	private HashMap<Point, String> listCoupTire;

	public Plateau(String epoque) {
		listCoupTire = new HashMap<Point, String>();
		listBateau = new ArrayList<Bateau>();
		creerFlotte(epoque);
		grille = new Object[TAILLE_GRILLE][TAILLE_GRILLE];
		for (int i = 0; i < grille.length; i++) {
			for (int j = 0; j < grille.length; j++) {
				grille[i][j] = null;
			}
		}
	}

	/**
	 * Creer la flotte de bateau en fonction de l'epoque
	 * 
	 * @param epoque, epoque de la flotte
	 */
	private void creerFlotte(String epoque) {
		FlotteFactory flotte;
		switch (epoque) {
		case FlotteFactory.FLOTTE_XX:
			flotte = FlotteEpoqueXX.getInstance();
			listBateau = flotte.formerFlotte();
			break;
		case FlotteFactory.FLOTTE_XV:
			flotte = FlotteEpoqueXV.getInstance();
			listBateau = flotte.formerFlotte();
			break;
		}
	}

	/**
	 * Methode qui permet de placer un bateau sur la grille selon le bateau et un
	 * tableau de 2 Point
	 * 
	 * @param lisBatoPoint, couple bateau et tableau de points
	 */
	public void placerBateau(Bateau bateau, Point[] tabPoint) {
		try {
			if (tabPoint.length != 2) {
				tabPoint = null;
			}
			// Verif bateau a faire
			Point a = tabPoint[0];
			Point b = tabPoint[1];
			//Verif qui il y a pas deja un bateau a cet endroit
			boolean onPeutPlacer = verifierPasDejaBateau(bateau, a, b);
			// verif distance entre les 2 points == la taille du bateau et
			if (a.distance(b) + 1 == bateau.getTaille() && onPeutPlacer) {
				boolean res = bateau.positionnerBateau(tabPoint);
				if (res) {
					Point[] tabP = bateau.getPos();
					// ajoute le bateau a la grille
					for (int i = 0; i < tabP.length; i++) {
						Point p = tabP[i];
						grille[(int) p.getX()][(int) p.getY()] = bateau;
					}
				}
			}
		} catch (NullPointerException e) {
			if (bateau == null) {
				System.out.println("Le bateau est null");
			} else if (tabPoint == null) {
				System.out.println("Tableau de point est null");
			}
		}

	}

	public boolean verifierPasDejaBateau(Bateau bateau, Point a, Point b) {
		// verif pas deja un bateau
		boolean onPeutPlacer = true;
		Point[] tabPointPosBateauAPlacer = bateau.trouverPointFrom2Point(a, b);
		if (tabPointPosBateauAPlacer != null) {
			int i = 0;
			while (onPeutPlacer && i < tabPointPosBateauAPlacer.length) {
				Object o = grille[(int) tabPointPosBateauAPlacer[i].getX()][(int) tabPointPosBateauAPlacer[i].getY()];
				if (o != null) {
					onPeutPlacer = false;
				}
				i++;
			}
		}else {
			onPeutPlacer = false;
		}
		return onPeutPlacer;
	}

	/**
	 * Methode qui permet de jouer un coup cherche un bateau sur la grille depuis le
	 * point en parametres appel la methode perdreVie de bateau si il y a un bateau
	 * ajoute dans la liste des coups tire le coup jou� renvoi plouf si rate,
	 * touche si touche, coule si coule
	 * 
	 * @param caseVise
	 * @param bateauQuiTire
	 * @return
	 */
	public String jouer(Point caseVise, Bateau bateauQuiTire) {
		String touche = Plateau.PLOUF;
		int degat = bateauQuiTire.tirerBoulet();
		Bateau bateauCible = this.chercherBateau(caseVise);
		if (bateauCible != null) {
			bateauCible.perdreVie(degat);
			if (!bateauCible.etreCoule()) {
				touche = Plateau.TOUCHE;
			} else {
				touche = Plateau.COULE;
				mettreAJourListCoupTire(bateauCible);
				coulerBateau(bateauCible);
			}
		}
		// ajout le coup jouer
		listCoupTire.put(caseVise, touche);
		return touche;
	}

	private void mettreAJourListCoupTire(Bateau bateauCible) {
		for (int i = 0; i < bateauCible.getPos().length; i++) {
			listCoupTire.put(bateauCible.getPos()[i], Plateau.COULE);
		}
	}

	/**
	 * Permet de retirer un bateau coule de la liste de bateau du plateau
	 * et de le retirer de la grille
	 * @param bateauCible
	 */
	private void coulerBateau(Bateau bateauCible) {
		listBateau.remove(bateauCible);
		Point[] tabP = bateauCible.getPos();
		for (int i = 0; i < bateauCible.taille; i++) {
			this.grille[(int) tabP[i].getX()][(int) tabP[i].getY()] = null;
		}
	}

	/**
	 * Chercher un bateau dans la grille a partir d'un point
	 * 
	 * @param caseVise, Case cibl�
	 * @return le bateau si il ya un bateau sur le case, null si il n'y a rien
	 */
	public Bateau chercherBateau(Point caseVise) {
		Bateau b;
		b = (Bateau) grille[(int) caseVise.getX()][(int) caseVise.getY()];
		return b;
	}

	/**
	 * Methode qui renvoi la liste des bateaux p�sents sur le plateau
	 * 
	 * @return
	 */
	public ArrayList<Bateau> getListBateau() {
		return listBateau;
	}

	public HashMap<Point, String> getListCoupTire() {
		return listCoupTire;
	}

	public Object getCaseGrille(int x, int y) {
		return this.grille[x][y];
	}

	/**
	 * permet de tester si on a fini de placer tout les bateaux de la liste
	 * 
	 * @return vrai si tout les bateaux sont places, faux sinon
	 */
	public boolean estFiniPlacerbateau() {
		boolean fini = true;
		int i = 0;
		while (i < listBateau.size() && fini) {
			if (listBateau.get(i).getPos()[0] == null) {
				fini = false;
			}
			i++;
		}
		return fini;
	}

	public boolean aPerdu() {
		boolean perdu = true;
		for(Bateau b : listBateau) {
			if (b.getNbProjectiles() > 0){
				perdu = false;
				break;
			}
			
		}
		return perdu;
	}
}
