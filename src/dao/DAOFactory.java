
package dao;

public abstract class DAOFactory {

	public final static int TXT = 1;

	public abstract JeuDAO getJeuDAO();

	public static DAOFactory getAbstractDAOFactory(int type) throws Exception {
		switch (type) {
		case TXT:
			return TxtFactory.getInstance();
		default:
			throw new Exception();
		}

	}
}