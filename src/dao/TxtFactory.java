package dao;

public class TxtFactory extends DAOFactory {

	private static TxtFactory instance = null;

	private TxtFactory() {

	}

	@Override
	public JeuDAO getJeuDAO() {
		return JeuTxtDAO.getInstance();
	}

	public static TxtFactory getInstance() {
		if (instance == null)
			instance = new TxtFactory();
		return instance;
	}

}