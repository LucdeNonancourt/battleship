
package dao;

import model.Jeu;


public interface JeuDAO {

	public Jeu load(String file) throws Exception;

	public void save(Jeu jeu);
}