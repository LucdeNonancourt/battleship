package controller;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import model.*;

public class PlateauJoueurController implements ActionListener {
	
	private Jeu j;
	private Bateau bateauAPlacer;
	private Bateau bateauQuiTire;
	private Point[] positionBateau;

	public PlateauJoueurController(Jeu j2) {
		j = j2;
		bateauAPlacer = null;
		positionBateau = new Point[2];
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		//En fonction de l'état du jeu, on place les bateaux ou on en selectionne un :
		switch(j.getEtatPartie()) {
		case Jeu.ETAT_JOUEUR_PLACER_BATEAU:
			bateauAPlacer = j.recupererBateauAPlacer();
			placerBateau(e.getActionCommand());
			break;
		case Jeu.ETAT_JOUEUR_SELECTIONNER_BATEAU:
			String[] cmd = e.getActionCommand().split(":");
			Point p = new Point(Integer.parseInt(cmd[0])-1,Integer.parseInt(cmd[1])-1);
			j.recupererBateau(p);
			break;
			
		default:
			//Do nothing, c'est même etonnant qu'on puisse cliquer sur le truc, mais bon....
			break;
		}
	}

	private void placerBateau(String commande) {
		//On split la commande pour obtenir un point;
		String[] cmd = commande.split(":");
		Point p = new Point(Integer.parseInt(cmd[0])-1,Integer.parseInt(cmd[1])-1);
		//Si le tableau de point est vide, alors on sauvegarde ce point la et on attend, sinon on place le bateau sur le model
		if(positionBateau[0] == null) 
			positionBateau[0] = p;
		else {
			positionBateau[1] = p;
			j.placerBateauxJoueur(bateauAPlacer, positionBateau);
			bateauAPlacer = null;
			positionBateau = new Point[2];
		}
		
	}
	
	
	
	


}
