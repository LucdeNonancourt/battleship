package controller;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Jeu;

public class PlateauAdversaireController implements ActionListener {
	
	Jeu j;
	
	public PlateauAdversaireController(Jeu j2) {
		j = j2;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//En fonction de l'état du jeu, on place les bateaux ou on en selectionne un :
		switch(j.getEtatPartie()) {
				case Jeu.ETAT_JOUEUR_TIRER_BATEAU:
					String[] cmd = e.getActionCommand().split(",");
					Point p = new Point(Integer.parseInt(cmd[0])-1,Integer.parseInt(cmd[1])-1);
					j.jouerCoup(p);
					break;
				default:
					//Do nothing, c'est même etonnant qu'on puisse cliquer sur le truc, mais bon....
					break;
				}
	}

}
