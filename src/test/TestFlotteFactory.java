package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import model.Bateau;
import model.Caraque;
import model.FlotteEpoqueXV;
import model.FlotteFactory;

public class TestFlotteFactory {
	
	private ArrayList<Bateau> flotte;
	
	@Before
	public void creerDonnee() {
		FlotteFactory flotteXV = FlotteEpoqueXV.getInstance();
		flotte = flotteXV.formerFlotte();
	}

	@Test
	public void test_CreationFlotteXX() {
		assertEquals("Devrait etre un Galion", "Galion", flotte.get(0).toString());
		assertEquals("Devrait etre un Caraque", "Caraque", flotte.get(1).toString());
		assertEquals("Devrait etre un Caraque", "Caraque", flotte.get(2).toString());
	}
}
