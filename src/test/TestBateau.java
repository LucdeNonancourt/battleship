package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.Bateau;
import model.Caraque;
import model.Galion;

public class TestBateau {

	private Bateau b1;
	
	private Bateau b2;
	
	@Before
	public void creerBateau() {
		b1 = new Caraque();
		b2 = new Galion();
	}
	
	@Test
	public void test_PerdreVie_CasNormal() {
		assertEquals("Il devrait avoir 2 points de vie", 2, b1.getVie());
		b1.perdreVie(1);
		assertEquals("Il devrait avoir 1 point de vie", 1, b1.getVie());
	}
	
	@Test
	public void test_PerdreVie_CasDegatEntraineMort() {
		assertEquals("Il devrait avoir 2 points de vie", 2, b1.getVie());
		b1.perdreVie(2);
		assertEquals("Il devrait avoir 0 point de vie", 0, b1.getVie());
	}
	
	@Test
	public void test_TirerBoulet_CasNormal() {
		assertEquals("Il devrait avoir 20 projectile", 20, b1.getNbProjectiles());
		assertEquals("Il devrait y avoir ", 2, b1.tirerBoulet());
		assertEquals("Il devrait avoir 19 projectile", 19, b1.getNbProjectiles());
	}

}
