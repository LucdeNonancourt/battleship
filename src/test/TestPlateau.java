package test;

import static org.junit.Assert.*;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import model.Bateau;
import model.FlotteFactory;
import model.Plateau;

public class TestPlateau {
	
	private Plateau p;
	
	private ArrayList<Bateau> lisB;

	@Before
	public void initDonnees() {
		p = new Plateau(FlotteFactory.FLOTTE_XX);
		lisB = p.getListBateau();
	}
	
	
	@Test
	public void test_placerBateau_CasNormal() {
		Bateau b = lisB.get(0);
		Point[] tab = {new Point(0,0), new Point(0,2)};
		assertEquals("il devrait y avoir un objet null dans cette case", null, p.getCaseGrille(0, 0));
		p.placerBateau(b, tab);
		assertEquals("Il devrait y avoir un bateau dans cette case", b, p.getCaseGrille(0, 0));
	}
	
	@Test
	public void test_placerBateau_CasDejaUnBateauSurCettePos() {
		//placement premier bateau
		Bateau b1 = lisB.get(0);
		Point[] tab = {new Point(0,0), new Point(0,2)};
		p.placerBateau(b1, tab);
		
		//placement deuxieme bateau a la meme pos
		Bateau b2 = lisB.get(1);
		assertEquals("il devrait y avoir l'objet bateau b1 deja dans cette case", b1, p.getCaseGrille(0, 0));
		p.placerBateau(b2, tab);
		assertEquals("Il devrait toujours y avoir l'objet bateau b1 dans cette case", b1, p.getCaseGrille(0, 0));
	}
	
	public void test_placerBateau_CasBateauNull() {		
		Bateau b = null;
		Point[] tab = {new Point(0,0), new Point(0,2)};
		p.placerBateau(b, tab);
		assertEquals("Il devrait y avoir un objet null dans cette case", b, p.getCaseGrille(0, 0));
	}	
	

}
