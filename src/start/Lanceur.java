package start;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import controller.*;
import dao.DAOFactory;
import model.*;
import view.*;
import view.Menu;

public class Lanceur {

	public static void main(String [] args){	
		
		final Jeu j = new Jeu();
		
		JFrame fenetreJoueur = new JFrame();
		
		//menu
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Fichier");
		JMenuItem save = new JMenuItem("Save");
		JMenuItem load = new JMenuItem("Load");
		menu.add(save);
		menu.add(load);
		save.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					DAOFactory.getAbstractDAOFactory(DAOFactory.TXT).getJeuDAO().save(j);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		
		menuBar.add(menu);
		fenetreJoueur.setJMenuBar(menuBar);
	
		//On creer les deux plateaux et les vues correspondantes
		fenetreJoueur.setLayout(new BorderLayout());
		fenetreJoueur.add(new PlateauJoueur(j), BorderLayout.SOUTH);
		fenetreJoueur.add(new PlateauAdversaire(j), BorderLayout.CENTER);
		fenetreJoueur.pack();
		fenetreJoueur.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//On creer un menu (qui affichera le jeu une fois 
		Menu m = new Menu(fenetreJoueur, j);
		

		m.setVisible(true);
		m.pack();
	}
	
	
}
